ATTRIBUTION.TXT

Adapted from material in Dungeon World by Sage LaTorra and Adam Koebel:
	Skinchanger (from Druid class)
	Whisperer (from Druid class)
	Minstrel (from Bard class)
	Poisoner (from Thief class)
	Cleric (from Cleric class)
	Wizard (from Wizard class)
	Bearer (from Fighter class)
	Poison Cook (from Thief class)
	Wiseguy (from Thief, Bard, and Immolator classes)

Adapted from material in Class Warfare by Red Box Vancouver:
	Storyteller (from Bard compendium class)

Adaoted from material in The Perilous Wilds by Lampblack & Brimstone:
	Hunter (from Hunter class)

Adapted from material in Freebooters on the Frontier by Lampblack & Brimstone:
	Rogue (from Thief class)
	Dwarf (from Dwarf race)

Adapted from material in 安土竜's race classes:
	Gnome (from Gnome class)
        Dwarf (from Gnome class)
	Dwarf (from Goblin class)
	Goblin (from Goblin class)
	Goblin (from Gnome class)
	Elf (from Gnome class)

Adapted from material in Shadi Alhusary's Gnome class:
	Gnome
        Dwarf
	Goblin

Adapted from material in Dungeon World Advanced (v0.2):
        Druid (from Druid class)
