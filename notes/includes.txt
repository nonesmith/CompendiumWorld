An asterisk before a class name means that it has not yet been written.
A question mark after a class name means that it may or may not be included.


CORE CLASSES
Barbarian
Bard -> *Minstrel, *Storyteller
Cleric -> Cleric
Druid -> Skinchanger, Whisperer
Fighter -> *Bearer, ?
Immolator -> *Zealot, *Pyromancer/Elementalist?
Paladin
Ranger -> *Beastmaster, *Archer, ?
Thief -> Poisoner, *Rogue, *Thief?
Wizard -> Wizard

EXISTING CLASSES
Awakened Mind -> keep
Bearer -> *Bearer
Blob -> drop
Dragonslayer -> keep
Flux -> keep but rename
Landed Gentry -> keep
Wraithlander -> keep
Delver -> keep
Explorer -> keep but ?rename
Hunter -> keep
Leader -> *Leader
Scout -> keep
Bard -> *Storyteller
Caver -> keep
Engineer -> keep
Fool -> drop
Good Samaritan
Householder
Luminary
Merchant
Monstrous Heritage -> drop or rework
Sage -> *Sage (trim down)
Shopkeeper
Wayfarer
*Oathkeeper

OUR CLASSES
Beastmaster -> make
Braggart
Cleric
Cook
Dark Pact
Dimensionalist
Druid
Martial Artist
Minstrel -> make
Poisoner
Skinchanger
Sorcerer
Whisperer
Wizard
Zealot

THINGS TO MAKE
?Elementalist
!Armor Guy
Duelist / Swashbuckler
Berserker
Tinkerer / Artificer
?Archaeologist
?Scribe
?Secret Society Cultist
Rogue gets some urban exploration stuff


=====================
Classes to work on
Zealot
Rogue
Beastmaster
Bard
Minstrel
Bearer